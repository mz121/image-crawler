﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WallpapersApp.Models
{
    public class WallpaperDto
    {
        public int Id { get; set; }

        public string Title { get; set; }
        public decimal Scores { get; set; }
        public string Href { get; set; }
        public string ThumbnailHref { get; set; }
        public int CategoryId { get; set; }
    }
}
