﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WallpapersApp.Models
{
    public class WallpaperWithPageDto
    {
        public int TotalCount { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPage { get; set; }

        public List<WallpaperCheckDto> Wallpapers { get; set; }
    }
}
