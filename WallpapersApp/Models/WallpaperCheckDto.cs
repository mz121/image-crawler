﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WallpapersApp.Models
{
    public class WallpaperCheckDto
    {
        public string FilePath { get; set; }
        public string Title { get; set; }

        public string SourceId { get; set; }

        public string ResizedPath { get; set; }

        public WallpaperStatus Status { get; set; }
        public string SourceUrl { get; set; }
    }
}
