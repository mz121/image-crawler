﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WallpapersApp.Models
{
    public enum WallpaperStatus
    {
        NotDownloaded,
        Downloaded,
        Resized,
        HaveTiny
    }
}
