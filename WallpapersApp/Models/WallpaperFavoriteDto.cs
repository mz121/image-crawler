﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WallpapersApp.Models
{
    public class WallpaperFavoriteDto
    {
        [Required]
        public int WallpaperId { get; set; }

        [Required]
        [MaxLength(250)]
        public string PhoneIMEI { get; set; }
    }
}
