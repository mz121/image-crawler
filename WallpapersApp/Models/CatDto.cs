﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WallpapersApp.Models
{
    public class CatDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Images { get; set; }

        public string SampleImage { get; set; } = string.Empty;

        public int NumberOfImages { get; set; }

    }
}
