﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WallpapersApp.Models
{
    public class TagDto
    {
        public string Name { get; set; }
    }
}
