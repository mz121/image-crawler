﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WallpapersApp.Models
{
    public class WallpaperSingleDto
    {
        public int Id { get; set; }

        public string Title { get; set; }
        public string[] Tags { get; set; }
        public decimal Scores { get; set; }
        public int CategoryId { get; set; }
        public string Href { get; set; }
        public string ThumbnailHref { get; set; }

        public bool IsFavorite { get; set; }
    }
}
