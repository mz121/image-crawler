﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Swashbuckle.AspNetCore.Swagger;
using AutoMapper;
using Hangfire;
using WallpapersApp.Data;
using Microsoft.EntityFrameworkCore;
using WallpapersApp.Services;
using System.IO;
using Microsoft.Extensions.FileProviders;
using Hangfire.PostgreSql;
using Microsoft.AspNetCore.Authentication.Cookies;
using WallpapersApp.Filters;

namespace WallpapersApp
{
    public class Startup
    {
        private readonly int? _httpsPort;
        public Startup(IConfiguration configuration,
            IHostingEnvironment env)
        {
            Configuration = configuration;

            if (env.IsDevelopment())
            {
                var launchSettings = new ConfigurationBuilder()
                    .SetBasePath(env.ContentRootPath)
                    .AddJsonFile("Properties\\launchSettings.json")
                    .Build();
                _httpsPort = launchSettings.GetValue<int>("iisSettings:iisExpress:sslPort");
            }
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEntityFrameworkNpgsql().AddDbContext<WallpaperDbContext>(options => options.UseNpgsql(Configuration.GetConnectionString("wallpaperContext")));
            services.AddHangfire(config => config.UsePostgreSqlStorage(Configuration.GetConnectionString("wallpaperContext")));

            services.AddRouting(options => options.LowercaseUrls = true);

            services.AddCors();
            services.AddMvc(opt =>
            {
                //opt.SslPort = _httpsPort;
                //opt.Filters.Add(typeof(RequireHttpsAttribute));
                //opt.Filters.Add(typeof(LinkRewriterFilter));
                opt.ReturnHttpNotAcceptable = true;
            });


            // user defined services
            services.AddScoped<WallpaperRepo>();
            services.AddScoped<WallpaperTagRepo>();
            services.AddScoped<TagRepo>();
            services.AddScoped<CategoryRepo>();
            services.AddScoped<UserRepository>();
            services.AddScoped<DownloadImages>();
            services.AddScoped<ImageResizerService>();

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            services.AddScoped<IUrlHelper, UrlHelper>(implementationFactory =>
            {
                var actionContext = implementationFactory.GetService<IActionContextAccessor>().ActionContext;
                return new UrlHelper(actionContext);
            });

            services.AddAutoMapper();

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "WallpapersApi", Version = "v1" });
                options.DocInclusionPredicate((x, y) => !y.RelativePath.Contains("check"));
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = "/check/login";
                    options.LogoutPath = "/check/logout";
                    options.Cookie.Name = "wallpaperAppHangfire";
                    options.ExpireTimeSpan = TimeSpan.FromHours(1);
                    options.Cookie.Path = "/";
                });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, DownloadImages downloadImages, ImageResizerService imageResizer, WallpaperDbContext db)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            db.Database.Migrate();

            app.UseCors(builder =>
            {
                builder.AllowAnyHeader();
                builder.AllowAnyOrigin();
            });

            app.UseAuthentication();

            app.UseHangfireServer();
            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                AppPath = "wallpaper.chakavak.co",
                Authorization = new[] { new CustomAuthenticationFilter() }

            });
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "images", "fullhd")),
                RequestPath = "/images"
            });
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "images", "thumbs")),
                RequestPath = "/thumbs"
            }); 

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "WallpapersAPI V1");
            });
            RecurringJob.AddOrUpdate(() => downloadImages.TestDownload(1, false), Cron.MinuteInterval(20));
            RecurringJob.AddOrUpdate(() => imageResizer.Resize(), Cron.MinuteInterval(21));
            //RecurringJob.AddOrUpdate(() => imageResizer.MakeTiny(), Cron.MinuteInterval(22));
            var id = BackgroundJob.Schedule(() => downloadImages.TestDownload(500, true), TimeSpan.FromSeconds(30));
            var cId = BackgroundJob.ContinueWith(id, () => imageResizer.Resize());
            //BackgroundJob.ContinueWith(cId, () => imageResizer.MakeTiny());


        }

        public Task IncreasePageNum(ref int pageNum)
        {
            pageNum++;
            return Task.CompletedTask;
        }
    }
}
