﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WallpapersApp.Migrations
{
    public partial class Favorite : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_WallpaperScores_PhoneIMEI",
                table: "WallpaperScores");

            migrationBuilder.DropIndex(
                name: "IX_Wallpapers_SourceId",
                table: "Wallpapers");

            migrationBuilder.DropIndex(
                name: "IX_WallpaperFavorites_PhoneIMEI",
                table: "WallpaperFavorites");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Wallpapers",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Tags",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Categories",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.CreateIndex(
                name: "IX_WallpaperScores_PhoneIMEI",
                table: "WallpaperScores",
                column: "PhoneIMEI");

            migrationBuilder.CreateIndex(
                name: "IX_WallpaperFavorites_PhoneIMEI",
                table: "WallpaperFavorites",
                column: "PhoneIMEI");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_WallpaperScores_PhoneIMEI",
                table: "WallpaperScores");

            migrationBuilder.DropIndex(
                name: "IX_WallpaperFavorites_PhoneIMEI",
                table: "WallpaperFavorites");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Wallpapers",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Tags",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Categories",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WallpaperScores_PhoneIMEI",
                table: "WallpaperScores",
                column: "PhoneIMEI",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Wallpapers_SourceId",
                table: "Wallpapers",
                column: "SourceId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WallpaperFavorites_PhoneIMEI",
                table: "WallpaperFavorites",
                column: "PhoneIMEI",
                unique: true);
        }
    }
}
