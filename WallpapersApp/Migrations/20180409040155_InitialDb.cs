﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace WallpapersApp.Migrations
{
    public partial class InitialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Created = table.Column<DateTimeOffset>(nullable: false),
                    Modified = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Created = table.Column<DateTimeOffset>(nullable: false),
                    Modified = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Wallpapers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CategoryId = table.Column<int>(nullable: true),
                    Created = table.Column<DateTimeOffset>(nullable: false),
                    FilePath = table.Column<string>(nullable: true),
                    Modified = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    ResizedPath = table.Column<string>(nullable: true),
                    Resolution = table.Column<string>(nullable: true),
                    Score = table.Column<int>(nullable: false),
                    Size = table.Column<long>(nullable: false),
                    SourceId = table.Column<string>(nullable: true),
                    SourceUrl = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    TinyPath = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Wallpapers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Wallpapers_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WallpaperFavorites",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Created = table.Column<DateTimeOffset>(nullable: false),
                    Modified = table.Column<DateTimeOffset>(nullable: false),
                    PhoneIMEI = table.Column<string>(maxLength: 250, nullable: false),
                    WallpaperId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WallpaperFavorites", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WallpaperFavorites_Wallpapers_WallpaperId",
                        column: x => x.WallpaperId,
                        principalTable: "Wallpapers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WallpaperScores",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Created = table.Column<DateTimeOffset>(nullable: false),
                    Modified = table.Column<DateTimeOffset>(nullable: false),
                    PhoneIMEI = table.Column<string>(maxLength: 250, nullable: false),
                    Score = table.Column<int>(nullable: false),
                    WallpaperId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WallpaperScores", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WallpaperScores_Wallpapers_WallpaperId",
                        column: x => x.WallpaperId,
                        principalTable: "Wallpapers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WallpaperTags",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    TagId = table.Column<int>(nullable: false),
                    WallpaperId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WallpaperTags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WallpaperTags_Tags_TagId",
                        column: x => x.TagId,
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WallpaperTags_Wallpapers_WallpaperId",
                        column: x => x.WallpaperId,
                        principalTable: "Wallpapers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Categories_Name",
                table: "Categories",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WallpaperFavorites_PhoneIMEI",
                table: "WallpaperFavorites",
                column: "PhoneIMEI",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WallpaperFavorites_WallpaperId",
                table: "WallpaperFavorites",
                column: "WallpaperId");

            migrationBuilder.CreateIndex(
                name: "IX_Wallpapers_CategoryId",
                table: "Wallpapers",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Wallpapers_Name",
                table: "Wallpapers",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Wallpapers_SourceId",
                table: "Wallpapers",
                column: "SourceId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WallpaperScores_PhoneIMEI",
                table: "WallpaperScores",
                column: "PhoneIMEI",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WallpaperScores_WallpaperId",
                table: "WallpaperScores",
                column: "WallpaperId");

            migrationBuilder.CreateIndex(
                name: "IX_WallpaperTags_TagId",
                table: "WallpaperTags",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_WallpaperTags_WallpaperId",
                table: "WallpaperTags",
                column: "WallpaperId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WallpaperFavorites");

            migrationBuilder.DropTable(
                name: "WallpaperScores");

            migrationBuilder.DropTable(
                name: "WallpaperTags");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropTable(
                name: "Wallpapers");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
