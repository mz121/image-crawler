﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace WallpapersApp.Migrations
{
    public partial class UserMigrate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_WallpaperScores_PhoneIMEI",
                table: "WallpaperScores");

            migrationBuilder.DropIndex(
                name: "IX_WallpaperFavorites_PhoneIMEI",
                table: "WallpaperFavorites");

            migrationBuilder.DropColumn(
                name: "PhoneIMEI",
                table: "WallpaperScores");

            migrationBuilder.DropColumn(
                name: "PhoneIMEI",
                table: "WallpaperFavorites");

            migrationBuilder.AddColumn<string>(
                name: "DeviceId",
                table: "WallpaperScores",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "WallpaperScores",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeviceId",
                table: "WallpaperFavorites",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    Created = table.Column<DateTimeOffset>(nullable: false),
                    Modified = table.Column<DateTimeOffset>(nullable: false),
                    DeviceId = table.Column<string>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.UniqueConstraint("AK_Users_DeviceId", x => x.DeviceId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WallpaperScores_UserId",
                table: "WallpaperScores",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_WallpaperFavorites_DeviceId",
                table: "WallpaperFavorites",
                column: "DeviceId");

            migrationBuilder.AddForeignKey(
                name: "FK_WallpaperFavorites_Users_DeviceId",
                table: "WallpaperFavorites",
                column: "DeviceId",
                principalTable: "Users",
                principalColumn: "DeviceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WallpaperScores_Users_UserId",
                table: "WallpaperScores",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WallpaperFavorites_Users_DeviceId",
                table: "WallpaperFavorites");

            migrationBuilder.DropForeignKey(
                name: "FK_WallpaperScores_Users_UserId",
                table: "WallpaperScores");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropIndex(
                name: "IX_WallpaperScores_UserId",
                table: "WallpaperScores");

            migrationBuilder.DropIndex(
                name: "IX_WallpaperFavorites_DeviceId",
                table: "WallpaperFavorites");

            migrationBuilder.DropColumn(
                name: "DeviceId",
                table: "WallpaperScores");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "WallpaperScores");

            migrationBuilder.DropColumn(
                name: "DeviceId",
                table: "WallpaperFavorites");

            migrationBuilder.AddColumn<string>(
                name: "PhoneIMEI",
                table: "WallpaperScores",
                maxLength: 250,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PhoneIMEI",
                table: "WallpaperFavorites",
                maxLength: 250,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_WallpaperScores_PhoneIMEI",
                table: "WallpaperScores",
                column: "PhoneIMEI");

            migrationBuilder.CreateIndex(
                name: "IX_WallpaperFavorites_PhoneIMEI",
                table: "WallpaperFavorites",
                column: "PhoneIMEI");
        }
    }
}
