﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WallpapersApp.Entities
{
    public class Category : BaseEntity
    {
        public virtual ICollection<Wallpaper> Wallpapers { get; set; }
    }
}
