﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WallpapersApp.Entities
{
    public class User : BaseEntity
    {
        [Required]
        [MaxLength(250)]
        public string DeviceId { get; set; }

        public ICollection<WallpaperFavorite> Favorites { get; set; }
    }
}
