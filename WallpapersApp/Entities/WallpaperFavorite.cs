﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WallpapersApp.Entities
{
    public class WallpaperFavorite : BaseEntityForFavScore
    {
        public bool IsFavorite { get; set; }
    }
}
