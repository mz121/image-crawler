﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WallpapersApp.Entities
{
    public class BaseEntityForFavScore
    {
        public int Id { get; set; }

        [Required]
        public int WallpaperId { get; set; }
        public Wallpaper Wallpaper { get; set; }

        public User User { get; set; }
        public string DeviceId { get; set; }

        public DateTimeOffset Created { get; set; }
        public DateTimeOffset Modified { get; set; }

    }
}
