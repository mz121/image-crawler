﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WallpapersApp.Entities
{
    public class WallpaperTag
    {
        public int Id { get; set; }

        public int WallpaperId { get; set; }
        public virtual Wallpaper Wallpaper { get; set; }

        public int TagId { get; set; }
        public virtual Tag Tag { get; set; }
    }
}
