﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WallpapersApp.Models;

namespace WallpapersApp.Entities
{
    public class Wallpaper : BaseEntity
    {
        public string FilePath { get; set; }
        public string Title { get; set; }
        public long Size { get; set; }
        public string Resolution { get; set; }

        public string SourceId { get; set; }

        public string ResizedPath { get; set; }
        public string TinyPath { get; set; }

        public int Score { get; set; }

        public int? CategoryId { get; set; }
        public Category Category { get; set; }

        public WallpaperStatus Status { get; set; }
        public string SourceUrl { get; set; }

        public  ICollection<WallpaperTag> Tags { get; set; }
        public ICollection<WallpaperFavorite> Favorites { get; set; }
    }
}
