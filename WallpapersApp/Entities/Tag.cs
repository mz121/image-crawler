﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WallpapersApp.Entities
{
    public class Tag : BaseEntity
    {

        public virtual ICollection<WallpaperTag> Wallpapers { get; set; }

    }
}
