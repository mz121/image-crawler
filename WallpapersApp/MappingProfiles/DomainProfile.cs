﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WallpapersApp.Entities;
using WallpapersApp.Models;
using WallpapersApp.Services;

namespace WallpapersApp.MappingProfiles
{
    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMap<Wallpaper, WallpaperDto>()
                .ForMember(d => d.Href, opt => opt.MapFrom(s => $"/images/{s.Name}"))
                .ForMember(d => d.ThumbnailHref, opt => opt.MapFrom(s => $"/thumbs/{s.ResizedPath}"));
            CreateMap<Wallpaper, WallpaperCheckDto>().ReverseMap();

            CreateMap<Wallpaper, WallpaperSingleDto>()
                .ForMember(d => d.Href, opt => opt.MapFrom(s => $"/images/{s.Name}"))
                .ForMember(d => d.ThumbnailHref, opt => opt.MapFrom(s => $"/thumbs/{s.ResizedPath}"))
                .ForMember(d => d.Tags, opt => opt.MapFrom(s => s.Tags.Select(c => c.Tag.Name)));


            CreateMap<Tag, TagDto>();
            CreateMap<Category, CatDto>()
                .ForMember(d => d.Images, opt => opt.MapFrom(s => $"/api/categories/{s.Id}/wallpapers"))
                .ForMember(d => d.NumberOfImages, opt => opt.MapFrom(s => s.Wallpapers.Count))
                .ForMember(d => d.SampleImage, opt => opt.MapFrom(s => $"/thumbs/{s.Wallpapers.FirstOrDefault().ResizedPath}")).ReverseMap();

            CreateMap<WallpaperFavorite, WallpaperFavoriteDto>();
        }
    }
}
