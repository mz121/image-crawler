﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WallpapersApp.Models;
using WallpapersApp.Services;
using WallpapersApp.Helpers;
using AutoMapper;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.AspNetCore.Http;

namespace WallpapersApp.Controllers
{
    [Route("api/categories")]
    public class CategoryController : Controller
    {
        private CategoryRepo _catRepo;
        private WallpaperRepo _wallpaperRepo;
        private IUrlHelper _urlHelper;
        private IMapper _mapper;

        public CategoryController(CategoryRepo catRepo,
            WallpaperRepo wallpaperRepo,
            IUrlHelper urlHelper,
            IMapper mapper)
        {
            _catRepo = catRepo;
            _wallpaperRepo = wallpaperRepo;
            _urlHelper = urlHelper;
            _mapper = mapper;
        }

        [HttpGet(Name = nameof(GetCategories))]
        public async Task<ActionResult<List<CatDto>>> GetCategories()
        {
            var catsFromRepo = await _catRepo.GetCategories();

            var catsToReturn = _mapper.Map<List<CatDto>>(catsFromRepo);

            return Ok(catsToReturn);
        }

        [HttpGet("{id}/wallpapers")]
        public async Task<ActionResult<PagedList<WallpaperDto>>> GetCategroyWallpapers(int id, EntitiesResourceParameters parameters)
        {
            if (!(await _catRepo.EntityExists(id))) return NotFound();

            var wallpapersFromRepo = await _wallpaperRepo.GetCategoryWallpapers(id, parameters);

            var wallpapersToReturn = new
            {
                items = _mapper.Map<List<WallpaperDto>>(wallpapersFromRepo),
                totalCount = wallpapersFromRepo.TotalCount,
                totalPage = wallpapersFromRepo.TotalPages
            };

            return Ok(wallpapersToReturn);
        }
    }
}