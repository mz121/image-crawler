﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WallpapersApp.Models;
using WallpapersApp.Services;
using AutoMapper;
using System.Collections.Generic;
using System.IO;
using WallpapersApp.Helpers;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using Microsoft.AspNetCore.Http;
using WallpapersApp.Entities;

namespace WallpapersApp.Controllers
{
    [Route("api/wallpapers")]
    public class WallpapersController : Controller
    {
        private WallpaperRepo _wallpaperRepo;
        private IUrlHelper _urlHelper;
        private IMapper _mapper;

        public WallpapersController(WallpaperRepo wallpaperRepo,
            IUrlHelper urlHelper,
            IMapper mapper)
        {
            _wallpaperRepo = wallpaperRepo;
            _urlHelper = urlHelper;
            _mapper = mapper;
        }

        [HttpGet(Name = nameof(GetWallpapers))]
        public async Task<ActionResult<PagedList<WallpaperDto>>> GetWallpapers([FromQuery]WallpapersResourceParameters parameters)
        {
            var wallpapersFromRepo = await _wallpaperRepo.GetWallpapers(parameters);
            var wallpapersToReturn = new
            {
                items = _mapper.Map<List<WallpaperDto>>(wallpapersFromRepo),
                totalCount = wallpapersFromRepo.TotalCount,
                totalPage = wallpapersFromRepo.TotalPages
            };
            return Ok(wallpapersToReturn);
        }
        
        [HttpPost("favorite", Name = nameof(MakeWallpaperFavorite))]
        public async Task<IActionResult> MakeWallpaperFavorite([FromBody]WallpaperFavoriteDto dto)
        {
            if (dto == null) return BadRequest();
            if (!(await _wallpaperRepo.EntityExists(dto.WallpaperId))) return NotFound();

            try
            {
                var result = await _wallpaperRepo.MakeWallpaperFavorite(dto);
                var response = new
                {
                    Id = result.WallpaperId,
                    PhonIMEI = result.User.DeviceId,
                    result.IsFavorite
                };
                return Ok(response);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { message = ex.Message, inner = ex.InnerException.Message });
            }
        }

        [HttpGet("{id}/{imei?}", Name = "GetWallpaper")]
        public async Task<ActionResult<WallpaperSingleDto>> GetWallpaper(int id, string imei)
        {
            if (id == 0) return BadRequest("id is empty");
            if (imei == null) return BadRequest("imei is empty");

            var wallpaper = await _wallpaperRepo.GetWallpaperViaIdAndImei(id, imei);
            if (wallpaper == null) return BadRequest("wallpaper not found");

            return Ok(wallpaper);
        }
    }
}