﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WallpapersApp.Services;
using AutoMapper;
using WallpapersApp.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;

namespace WallpapersApp.Controllers
{
    [Route("check")]
    public class CheckController : Controller
    {
        private WallpaperRepo _wallpaperRepo;
        private IMapper _mapper;

        public CheckController(WallpaperRepo wallpaperRepo, IMapper mapper)
        {
            _wallpaperRepo = wallpaperRepo;
            _mapper = mapper;
        }

        [HttpGet("wallpapers")]
        public async Task<IActionResult> CheckWallpapers(int pageSize = 20, int pageNumber = 1)
        {
            var wallpapersFromRepo = await _wallpaperRepo.GetEntities();
            var dto = new WallpaperWithPageDto
            {
                TotalCount = wallpapersFromRepo.Count(),
                CurrentPage = pageNumber,
                TotalPage = wallpapersFromRepo.Count() / pageSize,

            };
            dto.Wallpapers = new List<WallpaperCheckDto>(_mapper.Map<List<WallpaperCheckDto>>(
                wallpapersFromRepo
                .Skip(pageSize * (pageNumber - 1))
                .Take(pageSize)
                .ToList()));


            return Ok(dto);
        }

        [HttpGet("login")]
        public async Task<IActionResult> Login(string username, string password)
        {
            username = username ?? string.Empty;
            password = password ?? string.Empty;
            if (username.Equals("hangfireadmin") && password.Equals("mass12345.@$"))
            {
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(),
                    new AuthenticationProperties
                    {
                        IsPersistent = true
                    });

                return Content("you have been logged in! see wallpaper.chakavak.co/hangfire");
            }

            return Content("wrong user or pass!");
        }

        [HttpGet("logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return Content("you have logged out!");
        }

    }
}