﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WallpapersApp.Data;
using WallpapersApp.Entities;

namespace WallpapersApp.Services
{
    public abstract class BaseRepo<T> : IBaseRepo<T> where T : BaseEntity
    {
        private WallpaperDbContext _dbContext;
        private DbSet<T> entities;

        public BaseRepo(WallpaperDbContext dbContext)
        {
            _dbContext = dbContext;
            entities = _dbContext.Set<T>();
        }

        public async Task<T> AddEntity(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entity.Created = DateTimeOffset.Now;
            entity.Modified = DateTimeOffset.Now;
            var r = await entities.AddAsync(entity);
            try
            {
                var s = await _dbContext.SaveChangesAsync();
            }catch(Exception e)
            {
                var message = e.Message;
            }
            return entity;
        }

        public void DeleteEntity(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Remove(entity);
        }

        public async Task<bool> EntityExists(int id)
        {
            return (await entities.AnyAsync(e => e.Id == id));
        }

        public Task<IQueryable<T>> GetEntities()
        {
            return Task.Run(() =>
            {
                return entities.AsQueryable();
            });
        }

        public async Task<T> GetEntity(int id)
        {
            return await entities.AsNoTracking().FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<bool> Save()
        {
            return (await _dbContext.SaveChangesAsync() > 0);
        }

        public async Task Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entity.Modified = DateTimeOffset.Now;
            entities.Attach(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }
    }
}
