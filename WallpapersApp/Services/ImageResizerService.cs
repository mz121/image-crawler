﻿using Hangfire;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WallpapersApp.Data;
using WallpapersApp.Entities;

namespace WallpapersApp.Services
{
    public class ImageResizerService
    {
        private WallpaperRepo _wallpaperRepo;
        private WallpaperDbContext _dbContext;

        public ImageResizerService(WallpaperRepo wallpaperRepo,
            WallpaperDbContext dbContext,
            ILogger<ImageResizerService> logger)
        {
            _wallpaperRepo = wallpaperRepo;
            _dbContext = dbContext;
            _outputPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images");
            _logger = logger;
        }

        private const string _resolution = "1920x1080";
        private string _outputPath;
        private ILogger<ImageResizerService> _logger;
        private const float _resizeFactor = 0.15f;
        private const float _tinyFactor = 0.01f;
        private const int _resizeQuality = 80;

        public async Task Resize()
        {
            var wallpapers = await _dbContext.Wallpapers
                .Where(w => w.Status == Models.WallpaperStatus.Downloaded).ToListAsync();
            foreach (var w in wallpapers)
            {
                if (!string.IsNullOrEmpty(w.ResizedPath) || w.Status == Models.WallpaperStatus.Resized) continue;

                var path = Path.Combine(Directory.GetCurrentDirectory(), "images/fullhd", w.Name);
                if (File.Exists(path))
                {
                    var bitmap = SKBitmap.Decode(path);
                    if (bitmap != null)
                    {
                        var resizeWidth = (int)Math.Round(bitmap.Width * _resizeFactor);
                        var resizeHeight = (int)Math.Round(bitmap.Height * _resizeFactor);
                        var toBitmap = new SKBitmap(resizeWidth, resizeHeight, bitmap.ColorType, bitmap.AlphaType);

                        var b = bitmap.Resize(toBitmap, SKBitmapResizeMethod.Lanczos3);

                        var image = SKImage.FromBitmap(toBitmap);
                        var size = new SKSizeI(resizeWidth / 3, resizeHeight);
                        var location = new SKPointI(resizeWidth / 3, 0);
                        var rect = SKRectI.Create(location, size);
                        var fileName = Guid.NewGuid().ToString() + ".jpg";
                        _outputPath = Path.Combine(Directory.GetCurrentDirectory(), "images/thumbs", fileName);
                        var crop = image.Subset(rect);
                        var cropData = crop.Encode(SKEncodedImageFormat.Jpeg, _resizeQuality);
                        using (var cropStream = new FileStream(_outputPath, FileMode.Create, FileAccess.Write))
                        {
                            cropData.SaveTo(cropStream);
                        }
                        w.ResizedPath = fileName;
                        w.Status = Models.WallpaperStatus.Resized;
                        cropData.Dispose();
                        image.Dispose();

                        bitmap.Dispose();
                        await _wallpaperRepo.Update(w);
                    }
                }
            }

        }

        public async Task MakeTiny()
        {
            var wallpapers = await _dbContext.Wallpapers
                .Where(w => w.Status == Models.WallpaperStatus.Resized).ToListAsync();
            foreach (var w in wallpapers)
            {
                if (!string.IsNullOrEmpty(w.TinyPath) || w.Status == Models.WallpaperStatus.HaveTiny) continue;

                var path = Path.Combine(Directory.GetCurrentDirectory(), "images/thumbs", w.ResizedPath);
                if (File.Exists(path))
                {
                    var bitmap = SKBitmap.Decode(path);
                    var resizeWidth = 20;
                    var resizeHeight = 20;
                    var toBitmap = new SKBitmap(resizeWidth, resizeHeight, bitmap.ColorType, bitmap.AlphaType);

                    var b = bitmap.Resize(toBitmap, SKBitmapResizeMethod.Lanczos3);

                    var image = SKImage.FromBitmap(toBitmap);
                    var size = new SKSizeI(resizeWidth, resizeHeight);
                    var location = new SKPointI((image.Width / 2) - (resizeWidth / 2), (image.Height / 2) - (resizeHeight / 2));
                    var rect = SKRectI.Create(location, size);
                    var fileName = Guid.NewGuid().ToString() + ".jpg";
                    _outputPath = Path.Combine(Directory.GetCurrentDirectory(), "images/tiny", fileName);
                    var crop = image.Subset(rect);
                    var cropData = crop.Encode(SKEncodedImageFormat.Jpeg, _resizeQuality);
                    try
                    {

                        using (var cropStream = new FileStream(_outputPath, FileMode.Create, FileAccess.Write))
                        {
                            cropData.SaveTo(cropStream);
                        }
                        w.TinyPath = fileName;
                        w.Status = Models.WallpaperStatus.HaveTiny;
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex.Message);
                    }

                    await _wallpaperRepo.Update(w);

                    cropData.Dispose();
                    image.Dispose();

                    bitmap.Dispose();
                }
            }
        }
    }
}
