﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WallpapersApp.Controllers;
using WallpapersApp.Data;
using WallpapersApp.Entities;
using WallpapersApp.Helpers;
using WallpapersApp.Models;

namespace WallpapersApp.Services
{
    public class CategoryRepo : BaseRepo<Category>
    {
        private WallpaperDbContext _dbContext;

        public CategoryRepo(WallpaperDbContext dbContext)
            : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> CategoryExists(string catName)
        {
            if (catName == null) return false;

            return await _dbContext.Categories
                .AnyAsync(c => c.Name.ToLowerInvariant().Equals(catName.ToLowerInvariant()));
        }

        public async Task<Category> GetCategory(string catName)
        {
            if (catName == null) return null;

            var cat = await _dbContext.Categories
                //.Include(c => c.Parent)
                .Where(c => c.Name.ToLowerInvariant().Equals(catName.ToLowerInvariant()))
                .FirstOrDefaultAsync();

            return cat;
        }

        public async Task<List<Category>> GetCategories()
        {
            var cats = _dbContext.Categories
                .Include(c => c.Wallpapers)
                .OrderByDescending(c => c.Id);

            return await cats.ToListAsync();
        }
    }
}
