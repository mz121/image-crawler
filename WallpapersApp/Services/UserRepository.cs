﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WallpapersApp.Data;
using WallpapersApp.Entities;

namespace WallpapersApp.Services
{
    public class UserRepository : BaseRepo<User>
    {
        public UserRepository(WallpaperDbContext dbContext) : base(dbContext)
        {

        }
    }
}
