﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WallpapersApp.Data;
using WallpapersApp.Entities;
using WallpapersApp.Models;
using WallpapersApp.Helpers;
using System.Threading;
using AutoMapper;
using Microsoft.Extensions.Logging;

namespace WallpapersApp.Services
{
    public class WallpaperRepo : BaseRepo<Wallpaper>
    {
        private WallpaperDbContext _dbContext;
        private IMapper _mapper;

        public WallpaperRepo(WallpaperDbContext dbContext,
            UserRepository userRepository,
            IMapper mapper,
            ILogger<WallpaperRepo> logger)
            : base(dbContext)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _logger = logger;
            _userRepository = userRepository;
        }
        private ILogger<WallpaperRepo> _logger;
        private UserRepository _userRepository;

        public async Task<bool> WallpaperExsits(string sourceId)
        {

            var r = await _dbContext.Wallpapers
                .AnyAsync(w => w.SourceId.Equals(sourceId));

            return r;
        }

        public async Task<WallpaperFavorite> MakeWallpaperFavorite(WallpaperFavoriteDto dto)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.DeviceId == dto.PhoneIMEI);
            if(user == null)
            {
                user = new User
                {
                    Name = dto.PhoneIMEI,
                    DeviceId = dto.PhoneIMEI,
                    Created = DateTimeOffset.Now,
                    Modified = DateTimeOffset.Now,
                };
                await _userRepository.AddEntity(user);
                await _userRepository.Save();
            }
            var c = await _dbContext.WallpaperFavorites.Include(x => x.User)
                    .Where(w => w.User.DeviceId.Equals(dto.PhoneIMEI) && w.WallpaperId == dto.WallpaperId)
                    .FirstOrDefaultAsync();

            if (c != null)
            {
                c.IsFavorite = c.IsFavorite ? false : true;
                c.Modified = DateTimeOffset.Now;
                _dbContext.WallpaperFavorites.Attach(c)
                    .State = EntityState.Modified;
                await _dbContext.SaveChangesAsync();
                _logger.LogInformation("wallpaper favorites changed");
                return c;

            }

            var wf = new WallpaperFavorite
            {
                Created = DateTimeOffset.Now,
                Modified = DateTimeOffset.Now,
                WallpaperId = dto.WallpaperId,
                IsFavorite = true,
                DeviceId = user.DeviceId
            };

            var r = await _dbContext.WallpaperFavorites.AddAsync(wf);
            await _dbContext.SaveChangesAsync();
            _logger.LogInformation("wallpaper favorites created");
            return wf;

        }

        public async Task<WallpaperSingleDto> GetWallpaperViaIdAndImei(int id, string imei)
        {
            var wallpaper = await _dbContext.Wallpapers
                .FirstOrDefaultAsync(w => w.Id == id);
            var isFav = await _dbContext.WallpaperFavorites.Include(x => x.User)
                .Where(w => w.User.DeviceId == imei && w.WallpaperId == id)
                .FirstOrDefaultAsync();

            if (wallpaper == null) return null;

            var wallpaperToReturn = _mapper.Map<WallpaperSingleDto>(wallpaper);

            wallpaperToReturn.IsFavorite = isFav != null ? isFav.IsFavorite : false;
            return wallpaperToReturn;
        }

        public async Task<PagedList<Wallpaper>> GetWallpapers(WallpapersResourceParameters parameters)
        {

            var collectionBeforePaging = _dbContext.Wallpapers.Where(w => w.Status == WallpaperStatus.Resized).OrderByDescending(w => w.Id);
            int count = await collectionBeforePaging.CountAsync();

            var wallpapers = collectionBeforePaging
                .Skip(parameters.PageSize * (parameters.PageNumber - 1))
                .Take(parameters.PageSize)
                .AsQueryable();
            return PagedList<Wallpaper>.Create(wallpapers, parameters.PageNumber, parameters.PageSize, count);
        }

        public async Task<PagedList<Wallpaper>> GetCategoryWallpapers(int catId, EntitiesResourceParameters parameters)
        {
            var wallpapers = _dbContext.Wallpapers
                   .Where(x => x.CategoryId == catId)
                   .Where(x => x.Status == WallpaperStatus.Resized)
                   .OrderByDescending(w => w.Id).AsNoTracking();

            var count = await wallpapers.CountAsync(w => w.ResizedPath != null);
            wallpapers = wallpapers
                .Skip(parameters.PageSize * (parameters.PageNumber - 1))
                .Take(parameters.PageSize);

            return PagedList<Wallpaper>.Create(wallpapers, parameters.PageNumber, parameters.PageSize, count);
        }

        public async Task<Wallpaper> GetWallpaperAsync(int id)
        {
            var wallpaper = await _dbContext.Wallpapers
                .Include(w => w.Tags)
                    .ThenInclude(t => t.Tag)
                .Where(w => w.Id == id)
                .FirstOrDefaultAsync();

            return wallpaper;
        }
    }
}
