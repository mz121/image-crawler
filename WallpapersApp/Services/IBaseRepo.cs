﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WallpapersApp.Entities;

namespace WallpapersApp.Services
{
    public interface IBaseRepo<T> where T : BaseEntity
    {
        Task<bool> Save();
        Task<bool> EntityExists(int id);
        Task<T> GetEntity(int id);
        Task<IQueryable<T>> GetEntities();
        Task<T> AddEntity(T entity);
        void DeleteEntity(T entity);
        Task Update(T entity);
    }
}
