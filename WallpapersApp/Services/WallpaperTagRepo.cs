﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WallpapersApp.Data;
using WallpapersApp.Entities;
using WallpapersApp.Helpers;
using WallpapersApp.Models;
using WallpapersApp.Services;

namespace WallpapersApp.Services
{
    public class WallpaperTagRepo
    {
        private WallpaperDbContext _dbContext;

        public WallpaperTagRepo(WallpaperDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IEnumerable<Wallpaper>> GetWallpapersByTagsSearchQuery(string[] tags)
        {
            if (!tags.Any()) return null;

            List<Wallpaper> wallpapers = new List<Wallpaper>();

            foreach (var tag in tags)
            {
                var tagFromDb = await _dbContext.Tags.Where(t => t.Name.Contains(tag)).FirstOrDefaultAsync();
                var result = _dbContext.WallpaperTags
                    .Include(wt => wt.Tag)
                    .Include(wt => wt.Wallpaper)
                    .Where(wt => wt.TagId == tagFromDb.Id);
                var wList = await result.Select(wt => wt.Wallpaper).ToListAsync();

                wallpapers.AddRange(wList);
            }

            return wallpapers.AsEnumerable();

        }

        public async Task<List<WallpaperDto>> GetWallpapersTagsByList(List<Wallpaper> wallpapers)
        {
            List<WallpaperDto> wallpapersToReturn = new List<WallpaperDto>();
            foreach(var w in wallpapers)
            {
                var Tags = await _dbContext.WallpaperTags
                    .Include(wt => wt.Tag)
                    .Where(wt => wt.WallpaperId == w.Id)
                    .Select(wt => wt.Tag)
                    .ToListAsync();
                var dto = new WallpaperDto
                {
                    Id = w.Id,
                    //Created = w.Created,
                    Href = $"~/wwwroot/images/{w.Name}",
                    Title = w.Title,
                };
                wallpapersToReturn.Add(dto);
            }

            return wallpapersToReturn;
        }

        public async Task<WallpaperTag> AddWallpaperToTag(int wallpaperId, int tagId)
        {
            if (!(await _dbContext.Wallpapers.AnyAsync(w => w.Id == wallpaperId))) return null;
            if (!(await _dbContext.Tags.AnyAsync(t => t.Id == tagId))) return null;

            var wallpaperTag = new WallpaperTag
            {
                WallpaperId = wallpaperId,
                TagId = tagId
            };
            await _dbContext.WallpaperTags.AddAsync(wallpaperTag);
            await _dbContext.SaveChangesAsync();
            return wallpaperTag;
        }
    }
}
