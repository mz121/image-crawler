﻿using Hangfire;
using HtmlAgilityPack;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using WallpapersApp.Entities;
using WallpapersApp.Helpers;
using WallpapersApp.Models;

namespace WallpapersApp.Services
{
    public class DownloadImages
    {
        public static string BaseUrl = "https://wallpapershome.com";
        protected WallpaperRepo _wallpaperRepo;
        private TagRepo _tagRepo;
        private WallpaperTagRepo _wallpaperTagRepo;
        private CategoryRepo _categoryRepo;
        private ImageResizerService _imageResizer;
        protected ILogger<DownloadImages> _logger;

        private const string _size = "1920x1080";

        public DownloadImages(WallpaperRepo wallpaperRepo,
            TagRepo tagRepo,
            WallpaperTagRepo wallpaperTagRepo,
            CategoryRepo categoryRepo,
            ImageResizerService imageResizer,
            ILogger<DownloadImages> logger)
        {
            _wallpaperRepo = wallpaperRepo;
            _tagRepo = tagRepo;
            _wallpaperTagRepo = wallpaperTagRepo;
            _categoryRepo = categoryRepo;
            _imageResizer = imageResizer;
            _logger = logger;
        }
        public async Task RecurringDownload(
            int downloadNumber,
            string sizeToDownload = "1920x1080",
            bool deep = false)
        {

            using (HttpClient hc = new HttpClient())
            {

                string urlToDownload = $"{BaseUrl}?page={downloadNumber}";

                var result = await hc.GetAsync(urlToDownload);
                var stream = await result.Content.ReadAsStreamAsync();

                HtmlDocument doc = new HtmlDocument();
                doc.Load(stream);
                HtmlNodeCollection links = doc.DocumentNode.SelectNodes("//a[@href]");
                HtmlNodeCollection images = doc.DocumentNode.SelectNodes("//img[@alt]");

                foreach (var link in links)
                {
                    if (link.ParentNode.ParentNode.Id != "pics-list") continue;

                    var attr = link.Attributes.FirstOrDefault(h => h.Name == "href");
                    var tempIds = attr.Value.Split(new char[] { '-' });
                    var id = tempIds[tempIds.Length - 1].Split(new char[] { '.' })[0];
                    if ((await _wallpaperRepo.WallpaperExsits(id))) continue;
                    var fileName = Guid.NewGuid().ToString() + ".jpg";
                    var wallpaperToAdd = new Wallpaper
                    {
                        Name = fileName,
                        SourceId = id,
                        Status = WallpaperStatus.NotDownloaded
                    };

                    await _wallpaperRepo.AddEntity(wallpaperToAdd);

                    var tagsbeforeSplite = link.ChildNodes.FirstOrDefault(h => h.Name == "span");
                    var tags = tagsbeforeSplite.InnerHtml.Split(new char[] { ',' });
                    var linkRouteVals = tags[0].Split(new char[] { ' ' });
                    List<string> tmp = new List<string>(tags);
                    tmp.RemoveAt(0);
                    tags = tmp.ToArray();

                    var catArray = attr.Value.Split(new char[] { '/', ' ' });
                    Category ParentCatToSave = null;
                    Category ChildCatToSave = null;

                    if (catArray.Length < 4)
                    {
                        ParentCatToSave = new Category { Name = catArray[catArray.Length - 2] };
                    }
                    else
                    {
                        ParentCatToSave = new Category { Name = catArray[catArray.Length - 3] };
                        //if (!string.IsNullOrEmpty(catArray[catArray.Length - 2]))
                        //{
                        //    ChildCatToSave = new Category { Name = catArray[catArray.Length - 2] };
                        //}
                    }



                    if (ShouldPassImage(ParentCatToSave.Name)) continue;

                    string linkToDownloadImage = "images/wallpapers/";
                    string wallpaperTitle = string.Empty;
                    foreach (var routeVal in linkRouteVals)
                    {
                        var normalVal = routeVal;
                        if (routeVal.Contains(":"))
                        {
                            var tempEnum = routeVal.Except(new char[] { ':' });
                            normalVal = string.Empty;
                            foreach (var st in tempEnum)
                            {
                                normalVal += st;
                            }
                        }
                        wallpaperTitle += normalVal;
                        normalVal.Trim();
                        linkToDownloadImage += $"{normalVal.ToLower().TrimStart().TrimEnd()}-";

                    }

                    linkToDownloadImage += $"{sizeToDownload}-";
                    foreach (var t in tags)
                    {
                        linkToDownloadImage += $"{t.ToLower().TrimStart().TrimEnd()}-";
                    }

                    linkToDownloadImage += $"{id}.jpg";


                    var path = Path.Combine(
                            Directory.GetCurrentDirectory(), "images/fullhd");


                    var response = await hc.GetAsync($"{BaseUrl}/{linkToDownloadImage}");
                    var media = MediaTypeHeaderValue.Parse("image/jpg");
                    var resMedia = response.Content.Headers.ContentType;
                    if (resMedia.Equals(media))
                    {
                        using (var contentStream = await response.Content.ReadAsStreamAsync())
                        {
                            try
                            {
                                var fileStream = new FileStream(Path.Combine(path, fileName), FileMode.Create, FileAccess.Write);

                                await contentStream.CopyToAsync(fileStream);


                                wallpaperToAdd.Title = wallpaperTitle;
                                wallpaperToAdd.FilePath = path;
                                wallpaperToAdd.Resolution = sizeToDownload;
                                wallpaperToAdd.Size = (long)(fileStream.Length / 1024.0m);
                                wallpaperToAdd.Status = WallpaperStatus.Downloaded;
                                wallpaperToAdd.SourceUrl = $"{BaseUrl}/{linkToDownloadImage}";
                                await _wallpaperRepo.Update(wallpaperToAdd);

                                if (!(await _categoryRepo.CategoryExists(ParentCatToSave.Name)))
                                {
                                    await _categoryRepo.AddEntity(ParentCatToSave);
                                }
                                else
                                {
                                    ParentCatToSave = await _categoryRepo.GetCategory(ParentCatToSave.Name);
                                }

                                //await _wallpaperCatRepo.AddWallpaperToCategory(wallpaperToAdd.Id, ParentCatToSave.Id);
                                if (!(await _categoryRepo.CategoryExists(ChildCatToSave.Name)))
                                {
                                    if (ChildCatToSave != null)
                                    {
                                        ChildCatToSave.Name = ChildCatToSave.Name.Trim();
                                        //ChildCatToSave.ParentId = ParentCatToSave.Id;
                                        await _categoryRepo.AddEntity(ChildCatToSave);
                                    }
                                }
                                else
                                {
                                    ChildCatToSave = await _categoryRepo.GetCategory(ChildCatToSave.Name);
                                }

                                //await _wallpaperCatRepo.AddWallpaperToCategory(wallpaperToAdd.Id, ChildCatToSave.Id);

                                tags = tagsbeforeSplite.InnerHtml.Split(new char[] { ',', ' ', ':' });
                                foreach (var t in tags)
                                {
                                    var tagToAdd = new Tag();
                                    if (!(await _tagRepo.TagExists(t)))
                                    {
                                        tagToAdd.Name = t.Trim().ToLowerInvariant();
                                        if (string.IsNullOrEmpty(tagToAdd.Name)) continue;

                                        await _tagRepo.AddEntity(tagToAdd);
                                    }
                                    else
                                    {
                                        tagToAdd = await _tagRepo.GetTag(t);
                                    }
                                    var wt = await _wallpaperTagRepo.AddWallpaperToTag(wallpaperToAdd.Id, tagToAdd.Id);
                                }

                            }
                            catch (IOException ex)
                            {
                                _logger.LogError(ex.Message);
                            }

                            catch (Exception ex)
                            {
                                _logger.LogError(ex.Message);
                            }
                        }
                    }
                }
            }

            if (deep)
            {
                if (downloadNumber > 1)
                {
                    BackgroundJob.Enqueue(() => _imageResizer.Resize());
                    --downloadNumber;
                    var pId = BackgroundJob.Schedule(() => RecurringDownload(downloadNumber, "1920x1080", true), TimeSpan.FromMinutes(10));
                }
                else if (downloadNumber <= 1)
                {
                    RecurringJob.AddOrUpdate(() => RecurringDownload(1, "1920x1080", false), Cron.MinuteInterval(30));
                    RecurringJob.AddOrUpdate(() => _imageResizer.Resize(), Cron.MinuteInterval(16));
                }
            }
        }


        public async Task TestDownload(int totalPages = 1, bool recursive = false)
        {
            var url = $"{BaseUrl}?page={totalPages}";
            using (HttpClient hc = new HttpClient())
            {
                var result = await hc.GetAsync(url);
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var stream = await result.Content.ReadAsStreamAsync();
                    HtmlDocument doc = new HtmlDocument();
                    doc.Load(stream);
                    var links = doc
                        .DocumentNode
                        .SelectNodes("//a[@href]")
                        .Where(l => l.ParentNode.ParentNode.Id == "pics-list").ToList();
                    HtmlNodeCollection images = doc.DocumentNode.SelectNodes("//img[@alt]");
                    foreach (var link in links)
                    {
                        var attr = link.Attributes.FirstOrDefault(h => h.Name == "href");
                        var tempIds = attr.Value.Split(new char[] { '-' });
                        var id = tempIds[tempIds.Length - 1].Split(new char[] { '.' })[0];
                        if ((await _wallpaperRepo.WallpaperExsits(id))) continue;
                        var fileName = Guid.NewGuid().ToString() + ".jpg";
                        var catArray = attr.Value.Split(new char[] { '/', ' ' });
                        Category CatToSave = null;
                        if (catArray.Length < 4)
                        {
                            CatToSave = new Category { Name = catArray[catArray.Length - 2] };
                        }
                        else
                        {
                            CatToSave = new Category { Name = catArray[catArray.Length - 3] };
                        }
                        if (ShouldPassImage(CatToSave.Name)) continue;
                        var catControll = await _categoryRepo.GetCategory(CatToSave.Name);
                        if (catControll != null)
                        {
                            CatToSave = catControll;
                        }
                        var wallpaperToAdd = new Wallpaper
                        {
                            Name = fileName,
                            SourceId = id,
                            Status = WallpaperStatus.NotDownloaded,
                            Category = CatToSave
                        };
                        await _wallpaperRepo.AddEntity(wallpaperToAdd);

                        var tagsbeforeSplite = link.ChildNodes.FirstOrDefault(h => h.Name == "span");
                        var tags = tagsbeforeSplite.InnerHtml.Split(new char[] { ',' });
                        var linkRouteVals = tags[0].Split(new char[] { ' ' });
                        List<string> tmp = new List<string>(tags);
                        tmp.RemoveAt(0);
                        tags = tmp.ToArray();
                        string linkToDownloadImage = "images/wallpapers/";
                        string wallpaperTitle = string.Empty;
                        foreach (var routeVal in linkRouteVals)
                        {
                            var normalVal = routeVal;
                            if (routeVal.Contains(":"))
                            {
                                var tempEnum = routeVal.Except(new char[] { ':' });
                                normalVal = string.Empty;
                                foreach (var st in tempEnum)
                                {
                                    normalVal += st;
                                }
                            }
                            wallpaperTitle += normalVal;
                            normalVal.Trim();
                            linkToDownloadImage += $"{normalVal.ToLower().TrimStart().TrimEnd()}-";

                        }
                        linkToDownloadImage += $"{_size}-";
                        foreach (var t in tags)
                        {
                            linkToDownloadImage += $"{t.ToLower().TrimStart().TrimEnd()}-";
                        }
                        linkToDownloadImage += $"{id}.jpg";
                        var path = Path.Combine(
                            Directory.GetCurrentDirectory(), "images/fullhd");
                        var response = await hc.GetAsync($"{BaseUrl}/{linkToDownloadImage}");
                        var media = MediaTypeHeaderValue.Parse("image/jpg");
                        var resMedia = response.Content.Headers.ContentType;
                        if (resMedia.Equals(media))
                        {
                            using (var contentStream = await response.Content.ReadAsStreamAsync())
                            {
                                try
                                {
                                    var fileStream = new FileStream(Path.Combine(path, fileName), FileMode.Create, FileAccess.Write);
                                    await contentStream.CopyToAsync(fileStream);
                                    
                                    wallpaperToAdd.Title = wallpaperTitle;
                                    wallpaperToAdd.FilePath = path;
                                    wallpaperToAdd.Resolution = _size;
                                    wallpaperToAdd.Size = (long)(fileStream.Length / 1024.0m);
                                    wallpaperToAdd.Status = WallpaperStatus.Downloaded;
                                    wallpaperToAdd.SourceUrl = $"{BaseUrl}/{linkToDownloadImage}";
                                    await _wallpaperRepo.Update(wallpaperToAdd);
                                    fileStream.Dispose();
                                    tags = tagsbeforeSplite.InnerHtml.Split(new char[] { ',', ' ', ':' });
                                    foreach (var t in tags)
                                    {
                                        var tagToAdd = new Tag();
                                        tagToAdd.Name = t.Trim().ToLowerInvariant();
                                        if (!string.IsNullOrEmpty(tagToAdd.Name))
                                        {
                                            var tagControll = await _tagRepo.GetTag(t);
                                            if (tagControll != null)
                                            {
                                                tagToAdd = tagControll;
                                            }
                                            else
                                            {
                                                await _tagRepo.AddEntity(tagToAdd);
                                            }
                                        }
                                        else
                                        {
                                            continue;
                                        }
                                        var wt = await _wallpaperTagRepo.AddWallpaperToTag(wallpaperToAdd.Id, tagToAdd.Id);
                                    }
                                }
                                catch (IOException ex)
                                {
                                    _logger.LogError(ex.Message);
                                }
                                catch (Exception ex)
                                {
                                    _logger.LogError(ex.Message);
                                }
                            }
                        }
                    }
                }
            }
            if (recursive)
            {
                if (totalPages > 1)
                {
                    BackgroundJob.Enqueue(() => _imageResizer.Resize());
                    --totalPages;
                    var pId = BackgroundJob.Schedule(() => TestDownload(totalPages, true), TimeSpan.FromSeconds(10));
                }
            }
        }

        private bool ShouldPassImage(string name)
        {
            if (ImageFilters.Categories.Contains(name.ToLowerInvariant())) return true;

            return false;
        }
    }
}
