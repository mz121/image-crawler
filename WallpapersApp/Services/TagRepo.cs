﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WallpapersApp.Data;
using WallpapersApp.Entities;

namespace WallpapersApp.Services
{
    public class TagRepo : BaseRepo<Tag>
    {
        private WallpaperDbContext _dbContext;
        public TagRepo(WallpaperDbContext dbContext)
            : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Tag> GetTag(string tagName)
        {
            if (tagName == null) return null;

            var tag = await _dbContext.Tags.Where(t => t.Name.ToLowerInvariant().Equals(tagName.Trim().ToLowerInvariant())).FirstOrDefaultAsync();

            return tag;
        }

        public async Task<bool> TagExists(string tagName)
        {

            return await _dbContext.Tags.AnyAsync(
                t => t.Name.ToLowerInvariant().Equals(tagName.ToLowerInvariant()));
        }
    }
}
