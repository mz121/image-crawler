﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WallpapersApp.Entities;

namespace WallpapersApp.Data
{
    public class WallpaperDbContext : DbContext
    {
        public WallpaperDbContext(DbContextOptions options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<Wallpaper>().HasIndex(w => w.Name).IsUnique();
            builder.Entity<User>().HasAlternateKey(x => x.DeviceId);
            builder.Entity<Category>().HasIndex(c => c.Name).IsUnique();
            builder.Entity<WallpaperFavorite>().HasOne(x => x.User).WithMany(x => x.Favorites).HasForeignKey(x => x.DeviceId).HasPrincipalKey(x => x.DeviceId);
        }
        public DbSet<Wallpaper> Wallpapers { get; set; }
        public DbSet<Category> Categories { get; set; }
		public DbSet<Tag> Tags { get; set; }
		public DbSet<User> Users { get; set; }
        public DbSet<WallpaperTag> WallpaperTags { get; set; }
        public DbSet<WallpaperScore> WallpaperScores { get; set; }
        public DbSet<WallpaperFavorite> WallpaperFavorites { get; set; }

    }
}
